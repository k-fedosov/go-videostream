package main

import (
	"fmt"
	"log"

	"videostream/pkg/config"
	"videostream/pkg/server"

	"github.com/caarlos0/env/v6"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"github.com/go-redis/redis"
)

func main() {
	if ok, err := run(); !ok {
		log.Fatal("Run error: ", err)
	}
}

func run() (ok bool, err error) {
	var cfg config.Config
	err = env.Parse(&cfg)
	if err != nil {
		return false, err
	}

	dbURL := fmt.Sprintf("%s:%s@(%s:%s)/%s", cfg.MySqlDB.User, cfg.MySqlDB.Password, cfg.MySqlDB.Host, cfg.MySqlDB.Port, cfg.MySqlDB.Database)
	mysqlDB, err := sqlx.Connect("mysql", dbURL)
	if err != nil {
		log.Fatalln(err)
	}

	redisDB := redis.NewClient(&redis.Options{
		Addr: cfg.RedisDB.Host + ":" + cfg.RedisDB.Port,
		Password: cfg.RedisDB.Password,
		DB: cfg.RedisDB.Database,
	})


	go checkingConvert(mysqlDB)

	convertChan := make(chan bool)
	go waitingConvert(convertChan, mysqlDB)

	srv := server.NewServer(&cfg, mysqlDB, redisDB, &convertChan)

	err = srv.Run()
	if err != nil {
		return false, err
	}

	return true, nil
}
