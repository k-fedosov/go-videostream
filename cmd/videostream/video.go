package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"

	"github.com/jmoiron/sqlx"
	"videostream/pkg/server"
)

type video struct {
	ID     int    `db:"id"`
	Path   string `db:"path"`
	Status string `db:"status"`
}

func convertVideo(db *sqlx.DB) {
	var vv []video

	// only one video can be converting
	err := db.Select(&vv,
		`SELECT id
		FROM videos
		WHERE status = "converting"
		LIMIT 1`)
	if err != nil {
		log.Printf("Convert video check error: %v", err)
		return
	}
	if len(vv) > 0 {
		return
	}

	// select first video to convert
	err = db.Select(&vv,
		`SELECT id, path, status
		FROM videos
		WHERE status = "new"
		ORDER BY id ASC
		LIMIT 1`)
	if err != nil {
		log.Printf("Convert video select error: %v", err)
		return
	}
	if len(vv) == 0 {
		return
	}

	// converting
	v := vv[0]
	_, err = db.Exec(
		`UPDATE videos
			SET status = "converting"
			WHERE id = ` + strconv.Itoa(v.ID))
	if err != nil {
		log.Printf("Convert video error: %v", err)
		return
	}

	videoHash := strings.Split(v.Path, "/")[2]
	videoFolder := server.RootDir() + "/videos/" + videoHash
	err = os.MkdirAll(videoFolder, 0755)
	if err != nil {
		log.Printf("Create folder to convert video error: %v", err)
		return
	}

	//cmd := exec.Command("ffmpeg", "-i", "../.." + v.Path, "-g", "60", "-hls_time", "2", "-hls_list_size", "0", "-hls_segment_size", "500000", "../../videos/"+ videoHash + "/video.m3u8")
	cmd := exec.Command("bash", "../../convert-video.sh", "../.." + v.Path, "../../videos/" + videoHash)
	output, err := cmd.CombinedOutput()
	if err != nil {
		_, err = db.Exec(
			`UPDATE videos
			SET status = "failed"
			WHERE id = ` + strconv.Itoa(v.ID))
		if err != nil {
			log.Printf("Convert video set failed status error: %v", err)
			return
		}

		log.Println(fmt.Sprint(err) + ": " + string(output))
		return
	}

	_, err = db.Exec(
		`UPDATE videos
		SET
			status = "saved",
			path = "/videos/` + videoHash + `"
		WHERE id = ` + strconv.Itoa(v.ID))
	if err != nil {
		log.Printf("Convert video set saved status error: %v", err)
		return
	}

	// delete tmp video
	err = os.Remove(server.RootDir() + v.Path)
	if err != nil {
		log.Printf("Delete tmp video error: %v", err)
		return
	}
	// delete tmp folder
	path := strings.Split(v.Path, "/")
	folderPath := strings.Join(path[:len(path) - 1], "/")
	err = os.Remove(server.RootDir() + folderPath)
	if err != nil {
		log.Printf("Delete tmp video folder error: %v", err)
		return
	}

}

func checkingConvert(db *sqlx.DB) {
	for {
		convertVideo(db)
		time.Sleep(1000 * time.Millisecond * 60)
	}
}

func waitingConvert(ch chan bool, db *sqlx.DB) {
	for {
		<-ch
		convertVideo(db)
	}
}
