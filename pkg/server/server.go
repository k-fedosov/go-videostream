package server

import (
	"crypto/md5"
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
	"videostream/pkg/config"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"
	"github.com/jmoiron/sqlx"
)

type server struct {
	config      *config.Config
	db          *sqlx.DB
	redis       *redis.Client
	router      *gin.Engine
	convertChan *chan bool
}

func NewServer(cfg *config.Config, mysqlDB *sqlx.DB, redisDB *redis.Client, convertChan *chan bool) *server {
	s := server{
		config: cfg,
		db:          mysqlDB,
		redis:       redisDB,
		router:      gin.Default(),
		convertChan: convertChan,
	}
	s.router.Use(cors.Default())

	s.router.GET("/", s.handleIndex)

	auth := s.router.Group("", s.authorize)
	auth.GET("/videos/:folder", s.handleVideoPlaylist)
	auth.GET("/videos/:folder/:file", s.handleVideoFile)
	auth.GET("/tmp/:folder/:file", s.handleTmpVideoFile)
	auth.POST("/upload", s.handleVideoUpload)

	return &s
}

func (s *server) Run() error {
	return s.router.Run(s.config.VideoSteam.Port)
}

func (s *server) authorize(c *gin.Context) {
	headers, ok := c.Request.Header["Authorization"]
	if !ok || (len(headers) == 0) {
		c.JSON(401, gin.H{"error": "Not authorized"})
		c.Abort()
	}

	headersBearer := strings.Split(headers[0], " ")[1]
	headersAuth := strings.Split(headersBearer, "|")
	headerUserID := headersAuth[0]
	headerToken := headersAuth[1]

	if headerToken == "" {
		c.JSON(401, gin.H{"error": "Not authorized"})
		c.Abort()
	}

	var tokenKey = "token_" + headerToken
	redisToken, err := s.redis.Get(tokenKey).Result()
	if (err == nil) && (redisToken == "true") {
		return
	}

	var token string
	err = s.db.Get(&token, "SELECT token FROM personal_access_tokens WHERE token = '"+ headerToken +"' AND tokenable_id = " + headerUserID)
	if err != nil {
		c.JSON(401, gin.H{"error": "Not authorized"})
		c.Abort()
	} else {
		s.redis.Set(tokenKey, "true", s.config.Main.RedisTime * time.Second).Err()
	}
}

func (s *server) handleIndex(c *gin.Context) {
	c.String(http.StatusOK, "Videostream Service")
}

func (s *server) handleVideoPlaylist(c *gin.Context) {
	c.File("../../videos/" + c.Param("folder") + "/playlist.m3u8")
	return
}

func (s *server) handleVideoFile(c *gin.Context) {
	fl := strings.Split(c.Param("file"), ".")
	format := fl[ len(fl)-1 ]
	if format != "ts" && format != "m3u8" {
		c.JSON(404, gin.H{"error": "File not found"})
		return
	}

	c.File("../../videos/" + c.Param("folder") + "/" + c.Param("file"))
	return
}

func (s *server) handleTmpVideoFile(c *gin.Context) {
	fl := strings.Split(c.Param("file"), ".")
	format := fl[ len(fl)-1 ]
	formats := strings.Split(s.config.Main.VideoFormats, "|")
	if !contains(formats, format) {
		c.JSON(404, gin.H{"error": "File not found"})
		return
	}

	c.File("../../tmp/" + c.Param("folder") + "/" + c.Param("file"))
	return
}

func (s *server) handleVideoUpload(c *gin.Context) {
	file, header, err := c.Request.FormFile("file")
	if err != nil {
		c.Error(err)
		c.JSON(400, gin.H{"error": "Bad upload"})
		return
	}

	defer file.Close()

	fl := strings.Split(header.Filename, ".")
	videoFormat := fl[ len(fl)-1 ]
	formats := strings.Split(s.config.Main.VideoFormats, "|")
	if len(fl) > 1 && !contains(formats, videoFormat) {
		c.Error(fmt.Errorf("Unknown file format"))
		c.JSON(400, gin.H{"error": "Unknown file format"})
		return
	}

	timeNow := time.Now()
	nanoseconds := timeNow.UnixNano()
	folder := fmt.Sprintf("%x", md5.Sum([]byte(strconv.FormatInt(nanoseconds, 10))))[:10]

	folderRelativePath := s.config.Main.VideoTmpPath + folder
	fileRelativePath := folderRelativePath + "/video." + videoFormat
	folderSavePath := RootDir() + folderRelativePath
	fileSavePath := RootDir() + fileRelativePath

	err = os.MkdirAll(folderSavePath, 0755)
	if err != nil {
		c.Error(err)
		c.JSON(400, gin.H{"error": "Create file directory"})
		return
	}

	f, err := os.OpenFile(fileSavePath, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		c.Error(err)
		c.JSON(400, gin.H{"error": "Create file directory"})
		return
	}
	defer f.Close()

	// Copy the file to the destination path
	io.Copy(f, file)

	err = os.Chmod(fileSavePath, 0644)
	if err != nil {
		c.Error(err)
		c.JSON(400, gin.H{"error": "Change mode of file"})
		return
	}

	if err != nil {
		c.Error(err)
		c.JSON(400, gin.H{"error": "Upload file"})
		return
	}

	result, err := s.db.Exec(`INSERT INTO videos (path, status) VALUES ("`+ fileRelativePath +`", "new")`)
	if err != nil {
		c.Error(err)
		c.JSON(400, gin.H{"error": "Insert db error: " + err.Error()})
		return
	}
	videoID, _ := result.LastInsertId()

	c.JSON(200, gin.H{
		"result":  "file uploaded",
		"id": videoID,
		"path": fileRelativePath,
	})

	*s.convertChan <- true
}

