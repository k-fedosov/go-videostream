package config

import (
	"time"
)

type Config struct {
	Main struct {
		RedisTime    time.Duration `env:"REDIS_TIME" envDefault:"5s"`
		VideoTmpPath string `env:"VIDEO_TMP_PATH" envDefault:"/tmp/"`
		VideoFormats string `env:"VIDEO_FORMATS" envDefault:"mp4"`
	}
	VideoSteam struct {
		Port string `env:"VIDEO_STREAM_PORT" envDefault:":6201"`
	}
	MySqlDB struct {
		Host     string `env:"MY_SQL_HOST" envDefault:"45.147.176.207"`
		Port     string `env:"MY_SQL_PORT" envDefault:"3306"`
		Database string `env:"MY_SQL_DB" envDefault:"academy_sta_db"`
		User     string `env:"MY_SQL_USER" envDefault:"academy_sta_usr"`
		Password string `env:"MY_SQL_PASSWORD" envDefault:"YTCHyse7qYMa1K7L"`
	}
	RedisDB struct {
		Host     string `env:"REDIS_HOST" envDefault:"localhost"`
		Port     string `env:"REDIS_PORT" envDefault:"6379"`
		Database int    `env:"REDIS_HOST" envDefault:"0"`
		Password string `env:"REDIS_PASSWORD" envDefault:""`
	}
}
