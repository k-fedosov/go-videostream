VIDEO_STREAM_PATH := ./cmd/videostream/main.go
VIDEO_STREAM_DIR := ./cmd/videostream

DOCKER_PATH := ./build/docker

OS_NAME := $(shell uname -s | tr A-Z a-z)
OS_ARH :=  $(shell arch | tr A-Z a-z)

dock-vss:
	swag init -dir=$(VIDEO_STREAM_DIR)

run-vss:
	go run $(VIDEO_STREAM_PATH)

start-vss-dev:
	docker-compose -f $(DOCKER_PATH)/docker-compose.yaml up -d --force-recreate --no-deps --build

start-vss-local:
	docker-compose -f $(DOCKER_PATH)/docker-compose-local.yaml up -d --force-recreate --no-deps --build

build-vss:
	go build -o bin/app-$(OS_NAME)-$(OS_ARH) $(VIDEO_STREAM_PATH)

build-all-vss:
	GOOS=freebsd GOARCH=386 go build -o .bin/app-freebsd-i386 $(VIDEO_STREAM_PATH)
	GOOS=linux GOARCH=386 go build -o .bin/app-linux-i386 $(VIDEO_STREAM_PATH)
	GOOS=windows GOARCH=386 go build -o .bin/app-windows-i386 $(VIDEO_STREAM_PATH)
	GOOS=darwin GOARCH=386 go build -o .bin/app-darwin-i386 $(VIDEO_STREAM_PATH)
	GOOS=freebsd GOARCH=amd64 go build -o .bin/app-freebsd-amd64 $(VIDEO_STREAM_PATH)
	GOOS=linux GOARCH=amd64 go build -o .bin/app-linux-amd64 $(VIDEO_STREAM_PATH)
	GOOS=windows GOARCH=amd64 go build -o .bin/app-windows-amd64 $(VIDEO_STREAM_PATH)
	GOOS=darwin GOARCH=amd64 go build -o .bin/app-darwin-amd64 $(VIDEO_STREAM_PATH)

clean-vss:
	rm -rf .bin/videostream